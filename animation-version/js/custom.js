

// $('.services-area .hu-services-slider .carousel-indicators .col-lg-4:nth-child(2) .active').on('click', (e) => {
//     $('.line-move').animate({
//         left: '50',
//         // transform: 'translateY(-50%)',
//     });
// });

$('.app-box').on('click', function(){
    $('.app-box.current').removeClass('current');
    $(this).addClass('current');
    $('.app-box.current').siblings().css({"display":"none"});
    $('.app-box.current .app-content').css({"display":"block"});
});



$( ".app-box" ).each(function(index) {
    $('.app-box').on('click', function(){
        $('.back-supp').css({"opacity":"1", left:"0", "transition":"all .5s", "display":"block"});
    });
});
$( ".carousel-indicators .active" ).each(function(index) {
    $('.carousel-indicators .active').on('click', function(){
        $('.app-box').removeClass('current');
        $('.app-box').css({"display":"block"});
        $('.app-box .app-content').css({"display":"none"});
        $('.back-supp').css({"display":"none"});
    });
});
$('.carousel-indicators button').on('click', function(){
    $('.back-supp').css({"display":"none"});
});
$('.back-supp').on('click', function(){
    $(this).hide();
    $('.app-box').removeClass('current');
    $('.app-box .app-content').css({"display":"none"});
    $('.app-box').css({"display":"block"});
});


// Random Class Add Remove
// setRandomClass();
// 	setInterval(function () {
// 		setRandomClass();
// 	}, 50000);

// 	function setRandomClass() {
// 		var teamList = $('.partner-boxes');
// 		var teamItem = teamList.find('.canvas-box');
// 		var number = teamItem.length;
// 		var random = Math.floor((Math.random() * number));
// 		if(teamItem.eq(random).hasClass('team__person_active')) {
// 			var random = random + 1
// 		}
// 		$('.team__person_active').addClass('team__person_old')
// 			.siblings().removeClass('team__person_old');
// 		teamItem.eq(random).addClass('team__person_active')
// 			.siblings().removeClass('team__person_active');
// 	}

setRandomClass();
setInterval(function () {
    setRandomClass();
}, 3000);

function setRandomClass() {
    var ul = $(".partner-boxes");
    var items = ul.find(".canvas-box");
    var number = items.length;
    var random = Math.floor((Math.random() * number));
    items.removeClass("special");
    items.eq(random).addClass("special");
    // items.addClass("special-first");
    // items.eq(random).removeClass("special-first");
}


// $(".canvas-box:nth-child(5).special").prev().css({"background-color": "#fff", "box-shadow": "0 0 19px 5px #00000017", "position": "relative", "z-index" : "99"});
// $(".canvas-box:nth-child(5).special").next().css({"background-color": "#fff", "box-shadow": "0 0 19px 5px #00000017", "position": "relative", "z-index" : "99"});

// Random Class Add Remove


//Scroll Header Sticky
$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $("#main-header").addClass("header-sticky");
    }
    else{
        $("#main-header").removeClass("header-sticky");
    }
   
});
//Scroll Header Sticky

// $(window).scroll(function(){
//     if ($(window).scrollTop() >= 300) {
//         $('nav').addClass('fixed-header');
//         $('nav div').addClass('visible-title');
//     }
//     else {
//         $('nav').removeClass('fixed-header');
//         $('nav div').removeClass('visible-title');
//     }
// });




// Scroll Locamotive Animation Js
const scroller = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true
})

gsap.registerPlugin(ScrollTrigger)
scroller.on('scroll', ScrollTrigger.update)
ScrollTrigger.scrollerProxy(
    '.container-set', {
        scrollTop(value) {
            return arguments.length ?
            scroller.scrollTo(value, 0, 0) :
            scroller.scroll.instance.scroll.y
        },
        getBoundingClientRect() {
            return {
                left: 0, top: 0, 
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }
)

// ScrollTrigger.create({
//     trigger: '.image-mask',
//     scroller: '.container',
//     start: 'top+=30% 50%',
//     end: 'bottom-=40% 50%',
//     animation: gsap.to('.image-mask', {backgroundSize: '120%'}),
//     scrub: 2,
//     // markers: true
// })
ScrollTrigger.addEventListener('refresh', () => scroller.update())
ScrollTrigger.refresh()  



// Scroll Locamotive Animation Js


$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});




if($(window).width() <= 768){
    if(('.projects-box-slider').length != 0){
        $('.projects-box-slider').addClass('owl-carousel owl-theme');
        $('.projects-box-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                577:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}



if($(window).width() <= 415){
    if(('.hu-it-serv').length != 0){
        $('.hu-it-serv').addClass('owl-carousel owl-theme');
        $('.hu-it-serv').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                    dots: false,
                }, 
                413:{
                    
                    items:2,
                    nav:true,
                    dots: false,
                }
                
            }
        });
    }
}


$('#nav-icon3').click(function(){
    $(this).toggleClass('open');
});


// Services Line Trigger
gsap.from(".line-move", {
    duration: 0.5,
    xPercent: 0
});
var FirstChild = $(".carousel-indicators .col-lg-4:nth-child(1) button");
var SecChild = $(".carousel-indicators .col-lg-4:nth-child(2) button");
var ThridChild = $(".carousel-indicators .col-lg-4:nth-child(3) button");
FirstChild.click(() => {
    gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 0
    });
});
SecChild.click(() => {
      gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 100
      });
    //   $('.container-set').css({"transform":"matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, -4694, 0, 1)"});
});
ThridChild.click(() => {
    gsap.to(".line-move", {
      duration: 0.5,
      xPercent: 200
    });
});
// Services Line Trigger
