
$(".dropdown-menu").mouseenter(function(){
  $(this).parent().addClass('hvr-active');
});
$(".dropdown-menu").mouseleave(function(){
    $(this).parent().removeClass('hvr-active');
});



// Service Tabs
$('.app-box').on('click', function(){
    $('.app-box.current').removeClass('current');
    $(this).addClass('current');
    $('.app-box.current').siblings().css({"display":"none"});
    $('.app-box.current .app-content').css({"display":"block"});
});



$( ".app-box" ).each(function(index) {
    $('.app-box').on('click', function(){
        $('.back-supp').css({"opacity":"1", left:"0", "transition":"all .5s", "display":"block"});
    });
});
// $( ".carousel-indicators .active" ).each(function(index) {
    $('.carousel-indicators .active').on('click', function(){
        $('.app-box').removeClass('current');
        $('.app-box').css({"display":"block"});
        $('.app-box .app-content').css({"display":"none"});
    });
// });


$('.back-supp').on('click', function(){
    $(this).hide();
    $('.app-box').removeClass('current');
    $('.app-box .app-content').css({"display":"none"});
    $('.app-box').css({"display":"block"});
});

$('.carousel-indicators button').on('click', function(){
    $('.back-supp').css({"display":"none"});
});

// Service Tabs








// Scroll Locamotive Animation Js
const scroller = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true
})
gsap.registerPlugin(ScrollTrigger)
scroller.on('scroll', ScrollTrigger.update)
ScrollTrigger.scrollerProxy(
    '.container', {
        scrollTop(value) {
            return arguments.length ?
            scroller.scrollTo(value, 0, 0) :
            scroller.scroll.instance.scroll.y
        },
        getBoundingClientRect() {
            return {
                left: 0, top: 0, 
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }
)
ScrollTrigger.create({
    trigger: '.image-mask',
    scroller: '.container',
    start: 'top+=30% 50%',
    end: 'bottom-=40% 50%',
    animation: gsap.to('.image-mask', {backgroundSize: '120%'}),
    scrub: 2,
    // markers: true

// Hedaer Sticky Cass Add remove with Gsap

trigger: ".scrollpanels",
start: "top top-=0",
endTrigger: ".about-us",
end: "9999999",
pin: true,
pinSpacing: false,
scrub: 1,
toggleClass: { className: "fixed", targets: "#info" }
// Hedaer Sticky Cass Add remove with Gsap

});
ScrollTrigger.addEventListener('refresh', () => scroller.update())
ScrollTrigger.refresh()  


// Scroll Locamotive Animation Js


// Project Slider
/$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});
// Project Slider




if($(window).width() <= 768){
    if(('.projects-box-slider').length != 0){
        $('.projects-box-slider').addClass('owl-carousel owl-theme');
        $('.projects-box-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                577:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}


// Services Box Slider
if($(window).width() <= 415){
    if(('.hu-it-serv').length != 0){
        $('.hu-it-serv').addClass('owl-carousel owl-theme');
        $('.hu-it-serv').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                    dots: false,
                }, 
                413:{
                    
                    items:1,
                    nav:true,
                    dots: false,
                }
                
            }
        });
    }
}
// Services Box Slider

// Menu Toggle
$('#nav-icon3').click(function(){
    $(this).toggleClass('open');
});
// Menu Toggle




// Services Line Trigger
gsap.from(".line-move", {
    duration: 0.5,
    xPercent: 0
});
var FirstChild = $(".carousel-indicators .col-lg-4:nth-child(1) button");
var SecChild = $(".carousel-indicators .col-lg-4:nth-child(2) button");
var ThridChild = $(".carousel-indicators .col-lg-4:nth-child(3) button");
FirstChild.click(() => {
    gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 0
    });
});
SecChild.click(() => {
      gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 100
      });
    //   $('.container-set').css({"transform":"matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, -4694, 0, 1)"});
});
ThridChild.click(() => {
    gsap.to(".line-move", {
      duration: 0.5,
      xPercent: 200
    });
});
// Services Line Trigger



// Bnner Wave Lines
var svgEl = document.querySelector('.animated-lines');

var randomRange = function(min, max) {
  return ~~(Math.random() * (max - min + 1)) + min
};

var numberOfLines = 10,
  lineDataArr = [];

var createPathString = function() {

  var completedPath = '',
    comma = ',',
    ampl = 5; // pixel range from 0, aka how deeply they bend

  for (var i = 0; i < numberOfLines; i++) {

    var path = lineDataArr[i];

    var current = {
      x: ampl * Math.sin(path.counter / path.sin),
      y: ampl * Math.cos(path.counter / path.cos)
    };

    var newPathSection = 'M' +
      // starting point
      path.startX +
      comma +
      path.startY +
      // quadratic control point
      ' Q' +
      path.pointX +
      comma +
      (current.y * 1.2).toFixed(2) + // 1.5 to amp up the bend a little
      // center point intersection
      ' ' +
      ((current.x) / 10 + path.centerX).toFixed(2) +
      comma +
      ((current.y) / 5 + path.centerY).toFixed(2) +
      // end point with quadratic reflection (T) (so the bottom right mirrors the top left)
      ' T' +
      path.endX +
      comma +
      path.endY;
    path.counter++;

    completedPath += newPathSection;

  };

  return completedPath;

};

var createLines = function() {

  var newPathEl = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
    // higher is slower
    minSpeed = 85,
    maxSpeed = 150;

  // create an arr which contains objects for all lines
  // createPathString() will use this array
  for (var i = 0; i < numberOfLines; i++) {

    var lineDataObj = {
      counter: randomRange(1, 800), // a broad counter range ensures lines start at different cycles (will look more random)
      startX: randomRange(-200, -90),
      startY: randomRange(40, 70),
      endX: randomRange(400, 420), // viewbox = 200
      endY: randomRange(10, 20), // viewbox = 120
      sin: randomRange(minSpeed, maxSpeed),
      cos: randomRange(minSpeed, maxSpeed),
      pointX: randomRange(80, 60),
      centerX: randomRange(70, 170),
      centerY: randomRange(80, 50)
    }

    lineDataArr.push(lineDataObj)

  }

  var animLoop = function() {
    newPathEl.setAttribute('d', createPathString());
    requestAnimationFrame(animLoop);
  }

  // once the path elements are created, start the animation loop
  svgEl.appendChild(newPathEl);
  animLoop();

};



createLines();

// Bnner Wave Lines






// $(window).scroll(function(){
//     var scrollheader  = $(window).scrollTop() > 50;
    
//     if(scrollheader){
//         // $("#main-header").addClass("header-sticky");
//         $('body').find('#main-header').css({
//             'position': 'fixed',
//             'top': '0',

//         });
//     }
//     else{
//         $('body').find('#main-header').css({
//             'position': 'absolute',
//             'top': '0',

//         });
//     }
   
// });